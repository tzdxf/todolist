// 全局组件创建
import {Task} from "./Task";

Vue.component('Item',{
    template: `
<div class="task-item" @mouseover="isHover=true" @mouseleave="isHover=false" >

    <span v-if="!task.state" class="circular-check" @click="task.changeState()">        
        <i v-show="isHover && !task.state" class="el-icon-check"></i>
    </span>
    <span v-else class="circular-check success" @click="task.changeState()">
        <i class="el-icon-success task-icon-success"></i>
    </span>
    
    <span class="task-content">{{task.content}}</span>
    
    <i class="el-icon-delete del" @click.stop="delItem"></i>
<!--    <button type="button" class="btn btn-danger del" >删除</button>-->
</div>  
    `,
    // props: ['task'],
    props: {
        task: {
            type: Task,
            required: false,
        }
    },
    data(){
        return {
            isHover: false
        }
    },
    methods:{
        delItem(){
            // this.$parent.delTask(this.task.id);
            this.$emit('del-task',this.task.id);
        },
        format(time){
            let date = new Date(time);
            return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
        }
    }
})
