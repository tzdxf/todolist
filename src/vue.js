import './component'
import {Task} from "./Task";

var app = new Vue({
    el: '#app',
    data:{
        content: '',
        taskList: [],
        inputState: false,
        intervalTime:new Date(),
        requestTime: new Date()
    },
    computed: {
        todoList(){
            return this.taskList.filter(item=>!item.state)
        },
        completeList(){
            return this.taskList.filter(item=>item.state);
        }
    },
    watch:{
        taskList:{
            deep: true,
            handler(val){
                this.saveLocal()
            }
        }
    },
    created(){
        this.getLocal()
        this.updateRequestTime()
        this.updateTime()

    },
    methods: {
        // 更新时间
        updateTime(){
            let that = this;
            setInterval(()=>{
                that.intervalTime = new Date();
            },1)
        },
        updateRequestTime(){
            requestAnimationFrame(this.updateRequestTime)
            this.requestTime = new Date();
        },
        // 添加任务
        addTask(e){
            if (this.content){
                let task = new Task(this.content);
                this.content = '';
                this.taskList.unshift(task);
            } else {
                alert("请输入待办任务")
            }
        },
        delTask(id){
            let index = this.taskList.findIndex(item=>item.id === id);
            this.taskList.splice(index,1);
        },

        // 本地存储
        saveLocal(){
            if (this.taskList){
                window.localStorage.setItem("todoList",JSON.stringify(this.taskList))
            } else {
                window.localStorage.removeItem("todoList")
            }
        },
        // 取回数据
        getLocal(){
            let data = window.localStorage.getItem("todoList");
            if (data){
                let list = JSON.parse(data);
                list.forEach(item=>{
                    let task = new Task();
                    task.init(item);
                    this.taskList.push(task)
                })
            }
        },
        focusInput(e){
            console.log("选中")
            this.inputState = true;
        },
        blurInput(e){
            console.log("失去焦点")
            this.inputState = false
        }
    },
})
