export class Task{

    id
    content             // 任务内容
    createTime          // 创建时间
    state = false       // 状态
    completeTime        // 完成时间


    constructor(content) {
        this.content = content;
        this.createTime = new Date();
        this.id = new Date().getTime();
    }

    init({id,content,createTime,state,completeTime}){
        this.id           = id;
        this.content      = content;
        this.createTime   = new Date(createTime);
        this.state        = state;
        this.completeTime = new Date(completeTime);
    }

    changeState(){
        if (this.state){
            this.cancelCompleteTask()
        } else {
            this.completeTask()
        }
    }

    // 完成任务
    completeTask(){
        this.state = true
        this.completeTime = new Date();
    }

    // 取消完成任务
    cancelCompleteTask(){
        this.state = false
        this.completeTime = null;
    }

}
